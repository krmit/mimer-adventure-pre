"use strict"

let colors = require("colors/safe");
let prompt = require("promptly");
const moment = require("moment");
const sprintf = require('sprintf-js').sprintf;

module.exports.player=function() {
	this._alternatives=[];
	
	this._convert = function(words) {
			let line = "";
		for(let word of words) {
			if(typeof word === "string") {
			    line+=word;
			}
			else if(Array.isArray(word)) {
				line+=this._convert(word);
			}
			else {
				let text = word.msg;
			    if(word.color !== undefined) {
					text = colors[word.color](text);
				}
				 if(word.fontWeight !== undefined) {
					 text = colors[word.fontWeight](text);
				}
                line+=text;
			}
	} 
	return line;
	}
	
	this.write = function(...words) {
	    let line = this._convert(words);
		console.log(line);
	}
	
	this.add = function(id, promise, ...words) {
		this._alternatives.push({
			 "id": id, 
		     "msg": this._convert(words),
		     "promise": promise
	});
	}
	
	this.question = function(msg) {
		for(let i = 0; i <  this._alternatives.length; i++) {
			console.log(" "+ colors.bold(i)+"  " + this._alternatives[i].msg);
		}
		const result = this._alternatives;
		this._alternatives=[];
	    return prompt.prompt(msg).then((selection)=>result[selection].promise);	
	}	
}
