"use strict"

let colors = require("colors/safe");
let prompt = require("promptly");
const moment = require("moment");
const sprintf = require('sprintf-js').sprintf;
const {player} = require("../game/player.js");

module.exports.player = class playerAIRandom extends player {
  
  constructor(name) {
    super(name);
    this._type="ai";
  }
  
      static info() {
		return ["A randome AI player"]
	}
	
	question(...msg) {
		const result = this._alternatives;
		const selection = Math.floor(Math.random()*this._alternatives.length);
		this._alternatives=[];
	    return Promise.resolve(selection).then((selection)=>result[selection].callback());	
	}	
}
