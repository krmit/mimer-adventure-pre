"use strict"

const {player} = require("../game/player.js");

const _convert = function(words) {
	    console.log(words);
	    let html_text = "";
		for(let word of words) {
			if(typeof word === "string") {
			    html_text+=word;
			}
			else if(Array.isArray(word)) {
				html_text+=_convert(word);
			}
			else {
				let classes = "";
			    if(word.color !== undefined) {
					classes += word.color + " ";
				}
				 if(word.fontWeight !== undefined) {
					classes += word.fontWeight + " ";
				}
                html_text+="<span class='"+classes+"' >" + word.msg + "</span>";
			}
	} 
	return html_text;
	} 

module.exports.player = class playerWeb extends player {
	
	constructor(name) {
	super(name);
	this._type="local";	
	this._term=null;
	this._alternatives=[];
    }
	
	static setTerminal(term) {
		playerWeb._term = term;
	}
	
	send(...msg){
		for(let p of player.all) {
			if(!(p.constructor.name === "playerWeb" && p._name !== "Host")) {
		        p.write(...msg);
		    }	
		}
	};
	
	static info() {
		return "A local player";
	}
	
	write(...words) {
		let shortMsg = true; 
	    if(typeof words[words.length-1] === "boolean") {
			shortMsg = words.pop();
		}
		
		let html_text = _convert(words);
	    playerWeb._before = playerWeb._before.then(()=> {
	    if(shortMsg) {
			html_text +="\n<button> ok!</button>";
			this._count_row=0;
			let node_list = this._append(html_text);
		    let button = node_list[node_list.length-2];
		    return new Promise( function(resolve){button.onclick = function(){return resolve(true);}});
	    }
		else {
		    this._append(html_text);
		    return Promise.resolve(true);		
		}
		});	
	}
	
	question(...msg) {
		playerWeb._before = playerWeb._before.then(()=> {
		
		if(msg.length!==0){
		    let html_text = _convert(msg);
		    console.log(html_text);
		    this._append(html_text); 
	    }
		
		let html_text = "";
		for(let i = 0; i <  this._alternatives.length; i++) {
			html_text += "<button value='"+i+"' >" + this._alternatives[i].msg+"</button></br>";
		}
		const result = this._alternatives;
		let buttons = this._append(html_text);
		let list_of_promise=[];

		for(let i = 0; i < buttons.length; i++) {
		    if(buttons[i].tagName === "BUTTON") {
			   list_of_promise.push(new Promise( function(resolve){ 
		       buttons[i].onclick = function(){console.log("Valt: "+this.value);resolve(this.value);};
		   }));
		    }
	    }
	    
	    this._alternatives=[];
	    return Promise.race(list_of_promise).then((selection)=>{return result[selection].callback()});
	    });
	return	playerWeb._before;	
	}
	
	input(msg, defultValue) {
		return playerWeb._before = playerWeb._before.then(()=> {
		if(msg.length!==0){
		    let html_text = _convert(msg);
		    console.log(html_text);
		    this._append(html_text);   
	    }
		let html_text = "<input type='text'></input></br>";
		let input = this._append(html_text)[0];
		input.placeholder = defultValue;
		input.focus();
		return new Promise( function(resolve){
	    input.onkeydown = (function(input){return function(event) {if(event.keyCode === 13){if(input.value ==="") {resolve(defultValue)}else{resolve(input.value)}}}})(input);
	});	
	})
}
	
	_append(html_text) {
		const newcontent = document.createElement('div');
		newcontent.innerHTML += html_text+"<br />";
		let result=[];
		while (newcontent.firstChild) {
			result.push(newcontent.firstChild);
		    playerWeb._term.appendChild(newcontent.firstChild);
		}
		
		result[result.length-1].scrollIntoView();
		return result;
	}		
}

module.exports.player._before = Promise.resolve();
