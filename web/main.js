"use strict"

const engine = require("../game/engine.js");
const world = require("../world/register.js");

window.onload = function() {   
        world.player.aiRandom = require("../ai/player-ai-random.js").player;
        world.player.local = require("./player.js").player;

        world.player.local.setTerminal(document.getElementById("terminal"));
        window.startGame(world);
        new engine(world.player.local, world);
}
