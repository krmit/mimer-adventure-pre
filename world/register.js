"use strict"

module.exports = {
	creatures: {
	  humanKnigth: require("../world/individual/humanKnigth.js"),
      goblin: require("../world/individual/goblinCommon.js"),
      humanPoorKnigth: require("../world/individual/humanPoorKnigth.js") 
    },
    player: {},
    definitions: {
		creatures: {
			creature: require("../world/creatures/creature.js"),
			humanoid: require("../world/creatures/humanoid.js"),
			human: require("../world/creatures/human.js"),
			goblin: require("../world/creatures/goblin.js"),
			troll: require("../world/creatures/troll.js"),
			trolla: require("../world/creatures/troll.js")
		},
		type: {
			knight: require("../world/type/knight.js"),
			archer: require("../world/type/archer.js"),
			pikemen: require("../world/type/pikemen.js"),
			nobel: require("../world/type/nobel.js"),
			poor: require("../world/type/poor.js")
		},
	},
   tools: {
	dice: require("../game/dice.js"),
	action: require("../game/action.js")
   },
   style: require("../game/style.js")
}
