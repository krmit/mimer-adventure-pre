"use strict";

const humanoid = require("./humanoid.js");
const action = require("../../game/action.js");
const { dice } = require("../../game/dice.js");

let creature = class troll extends humanoid {
  constructor(name) {
    super(
      name,
      dice(8, 15, 3),
      dice(5, 15, 2),
      dice(5, 15, 2),
      dice(1, 5, 2),
      dice(1, 3, 1),
      dice(1, 3, 1),
      dice(1, 10, 1),
      dice(1, 15, 2)
    );
    this.creature = "Troll";
    this.hp = 50 + Math.round((this.size * this.condition) / 2);
    this.cost += 250;
    this._description = "En helt vanligt dumt troll.";
    this._salute = "Äta mat?";
    this._lose = "grå grå gråååååååååååååååååååååå!";
    this._ask = "Du vill?";
    this._win = "Äta mat!";
  }

  hit(enemy) {
    const _this = this;

    const my_action = new action("En knock!");
    my_action.type.push("body");
    my_action.by = _this;
    my_action.target = enemy;
    my_action.maxDamage = 20 + this.strength * this.size;
    my_action.minDamage = 20;
    my_action.normalDistribution = 3;
    my_action.initiativ = this.flexibility;
    my_action.accuracy = 0.3 + this.flexibility / 10;
    return my_action;
  }

  head(enemy) {
    const _this = this;

    const my_action = new action("En spark!");
    my_action.type.push("body");
    my_action.by = _this;
    my_action.target = enemy;
    my_action.maxDamage = 30 + this.strength * this.size * 2;
    my_action.minDamage = 30;
    my_action.normalDistribution = 1;
    my_action.initiativ = 2 * this.flexibility + 5;
    my_action.accuracy = 0.1 + (2 * this.flexibility) / 10;
    return my_action;
  }
};

module.exports = creature;
