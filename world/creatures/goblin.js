"use strict";

const { red, yellow, bold } = require("../../game/style.js");
const humanoid = require("./humanoid.js");
const action = require("../../game/action.js");
const { dice } = require("../../game/dice.js");

let creature = class goblin extends humanoid {
  constructor(name) {
    super(
      name,
      dice(2, 6, 1),
      dice(1, 8, 2),
      dice(5, 10, 2),
      dice(5, 12, 2),
      dice(1, 10, 2),
      dice(1, 7, 2),
      dice(1, 7, 2),
      dice(1, 10, 2)
    );
    this.cost += 100;
    this.hp = 20 + Math.round((this.condition * this.condition) / 2);
    this.creature = "Goblin";
    this._description = "Goblin är en ful krigare.";
    this._salute = "ha ha ha! 👿 ";
    this._lose = "oof";
    this._ask = "Dina order?";
    this._win = "haaaaaa";
    this._title = "Goblin";
  }

  bite(enemy) {
    const _this = this;

    const my_action = new action("Ett bet");
    my_action.type.push("body");
    my_action.by = _this;
    my_action.target = enemy;
    my_action.maxDamage = 20;
    my_action.minDamage = 10;
    my_action.normalDistribution = 2;
    my_action.initiativ = this.flexibility;
    my_action.accuracy = 0.5 + this.flexibility / 20;
    return my_action;
  }

  bite_info() {
    const bite = this.bite(null);
    return (
      "Ett kraftigt bet. " +
      bite.minDamage +
      "-" +
      bite.maxDamage +
      " ❤️ " +
      Math.floor(bite.accuracy * 100) +
      "% 🎯"
    );
  }

  kick(enemy) {
    const _this = this;

    const my_action = new action("En spark!");
    my_action.type.push("body");
    my_action.by = _this;
    my_action.target = enemy;
    my_action.maxDamage = 5 + this.strength * 2;
    my_action.minDamage = 1;
    my_action.normalDistribution = 2;
    my_action.initiativ = 2 * this.flexibility + 5;
    my_action.accuracy = 0.7 + this.flexibility / 20;
    return my_action;
  }

  kick_info() {
    const kick = this.kick(null);
    return (
      "En hård spark." +
      kick.minDamage +
      "-" +
      kick.maxDamage +
      " ❤️ " +
      Math.floor(kick.accuracy * 100) +
      "% 🎯"
    );
  }

  static infoChooseMe() {
    return "Välj en Goblin";
  }

  static createRandom(name) {
    return Promise.resolve(new creature(name));
  }
};

module.exports = creature;
