"use strict";

const humanoid = require("./humanoid.js");
const action = require("../../game/action.js");
const { dice } = require("../../game/dice.js");

let creature = class human extends humanoid {
  constructor(name) {
    super(
      name,
      dice(5, 10, 3),
      dice(1, 10, 2),
      dice(1, 10, 2),
      dice(1, 10, 2),
      dice(1, 10, 2),
      dice(1, 10, 2),
      dice(1, 10, 2),
      dice(1, 10, 2)
    );
    this.creature = "Mäniska";
    this.hp = 50 + Math.round((this.size * this.condition) / 2);
    this.cost += 250;
    this._description = "En helt vanlig mäniska.";
    this._salute = " Jag kan inte slås!";
    this._lose = "nej!";
    this._ask = "Vad ska vi göra?";
    this._win = "Hur är detta möjligt?";
    this._title = "Mäniskan";
  }

  hit(enemy) {
    const _this = this;

    const my_action = new action("Ett slag");
    my_action.type.push("body");
    my_action.by = _this;
    my_action.target = enemy;
    my_action.maxDamage = 10;
    my_action.minDamage = 1;
    my_action.normalDistribution = 2;
    my_action.initiativ = this.flexibility;
    my_action.accuracy = 0.6 + this.flexibility / 10;
    return my_action;
  }

  hit_info() {
    const hit = this.hit(null);
    return (
      "Ett knytnävesslag. " +
      hit.minDamage +
      "-" +
      hit.maxDamage +
      " ❤️ " +
      Math.floor(hit.accuracy * 100) +
      "% 🎯"
    );
  }
};

module.exports = creature;
