"use strict";
const { red, green, bold } = require("../../game/style.js");

let creature = class creature {
  constructor(name) {
    this.cost = 0;
    this.hp = 0;
    this.type = "";
    this.name = name;
    this._title = "";
    this._description = "";
    this._salute = "";
    this._ask = "";
    this._lose = "";
    this._win = "";
  }

  nameTag() {
    return [green(this._title, " ", this.name), ": "];
  }

  salute() {
    return [this.nameTag(), "\n", this._salute];
  }

  ask() {
    return [this.nameTag(), this._ask];
  }

  win() {
    return [this.nameTag(), "\n", this._win];
  }

  lose() {
    return [this.nameTag(), "\n", this._lose];
  }

  status() {
    return [this.nameTag(), String(this.hp), bold(" ❤️")];
  }

  isFighting() {
    return this.hp > 0;
  }

  moves(enemy) {
    return [];
  }

  damage(action) {
    return ["Ingen skada"];
  }

  static infoChooseMe() {
    return "";
  }

  static createRandom() {
    return null;
  }
};

module.exports = creature;
