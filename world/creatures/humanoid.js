"use strict";

const { red, green, bold } = require("../../game/style.js");
const { action } = require("../../game/action.js");
const creature = require("./creature.js");

module.exports = class humanoid extends creature {
  constructor(
    name,
    size,
    condition,
    strength,
    flexibility,
    intelligent,
    charisma,
    wisdom,
    might
  ) {
    super(name);
    this.cost += 250;
    this.size = size;
    this.strength = strength;
    this.condition = condition;
    this.flexibility = flexibility;
    this.intelligent = intelligent;
    this.charisma = charisma;
    this.wisdom = wisdom;
    this.might = might;
    this._protection = 0;
    this._armor = 0;
  }

  statusOfStats() {
    const _this = this;

    return [
      this.nameTag(),
      "\n",
      bold(this.creature, "/", this.type),
      "\n\n",
      "Cost        ",
      String(this.cost),
      " 💰\n",
      "HP          ",
      String(this.hp),
      " ❤️\n\n",
      "Size        ",
      String(this.size),
      " 🔺\n",
      "Strength    ",
      String(this.strength),
      " 💪🏼\n",
      "Condition   ",
      String(this.condition),
      " 🏃‍\n",
      "Flexibility ",
      String(this.flexibility),
      " 💃🏻\n",
      "Intelligent ",
      String(this.intelligent),
      " ♟\n",
      "Charisma    ",
      String(this.charisma),
      " 🗣 \n",
      "Wisdom      ",
      String(this.wisdom),
      " 📚\n",
      "Might       ",
      String(this.might),
      " ❗️\n",
      "\n"
    ];
  }

  damage(action) {
    const tmp_accuracy = action.accuracy - this._protection;
    if (Math.random() > tmp_accuracy) {
      return ["Ha! Du missade! (" + Math.floor(tmp_accuracy * 100) + "% 🎯)"];
    } else {
      let hp = 0;
      for (let i = 0; i < action.normalDistribution; i++) {
        hp +=
          Math.floor(
            (action.maxDamage - action.minDamage + 1) * Math.random()
          ) + action.minDamage;
      }
      hp = Math.round(hp / action.normalDistribution);
      this.hp -= hp;
      return [
        "Aj, jag tog ",
        red(hp),
        " i skada. (" + Math.floor(tmp_accuracy * 100) + "% 🎯)"
      ];
    }
  }
};
