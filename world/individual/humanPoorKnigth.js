"use strict";

const { red, green, bold } = require("../../game/style.js");
const human = require("../creatures/human.js");
const knight = require("../type/knight.js");
const poor = require("../type/poor.js");

let creature = class humanKnight extends poor(knight(human)) {
  constructor(name) {
    super(name);
    this.type = "fattig Riddare";
    this._description =
      "Riddaren är en mäktig krigare i från Småland. " +
      "Ridaren gör stor skada genom att hugga med sitt " +
      "svärd. Den billiga rustningen som är köpt på IKEA skyddar" +
      "ibland mot visa attacker.";
    this._salute =
      "Jag ska rädda dig ifrån detta hemska monster, om jag har råd!";
    this._lose = "Ack och ve, nu har jag inge pengar kvar igen!";
    this._win = "De fattiga har segrad!";
  }

  moves(enemy) {
    this._protection = 0;
    return [
      ["cut", () => this.cut(enemy), this.cut_info()],
      ["fendOff", () => this.fendOff(enemy), this.fendOff_info()]
    ];
  }

  static infoChooseMe() {
    return "Välj en mänsklig fattig riddare";
  }

  static createRandom(name) {
    return Promise.resolve(new creature(name));
  }
};

module.exports = creature;
