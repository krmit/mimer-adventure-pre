"use strict";

const { red, green, bold } = require("../../game/style.js");
const goblin = require("../creatures/goblin.js");

let creature = class goblinCommon extends goblin {
  constructor(name) {
    super(name);
    this.type = "Vanlig";
  }

  moves(enemy) {
    return [
      ["bite", () => this.bite(enemy), this.bite_info()],
      ["kick", () => this.kick(enemy), this.kick_info()]
    ];
  }

  static infoChooseMe() {
    return "Välj en helt vanlig goblin.";
  }

  static createRandom(name) {
    return Promise.resolve(new creature(name));
  }
};

module.exports = creature;
