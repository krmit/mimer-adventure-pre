"use strict";
const { red, green, bold } = require("../../game/style.js");
const human = require("../creatures/human.js");
const knight = require("../type/knight.js");
let creature = class humanKnight extends knight(human) {
  constructor(name) {
    super(name);
    this.type = "Riddare";
    this._title = "Riddare";
  }
  moves(enemy) {
    this._protection = 0;
    return [
      ["cut", () => this.cut(enemy), this.cut_info()],
      ["fendOff", () => this.fendOff(enemy), this.fendOff_info()]
    ];
  }
  static infoChooseMe() {
    return "Välj en mänsklig mäktig riddare";
  }
  static createRandom(name) {
    return Promise.resolve(new creature(name));
  }
};
module.exports = creature;
