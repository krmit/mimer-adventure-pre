"use strict";

const { red, green, bold } = require("../../game/style.js");

let creature = base =>
  class poor extends base {
    constructor(name) {
      super(name);
      this.cost -= 250;
      this.size -= 2;
      if (this.size < 1) {
        this.size = 1;
      }
      this.strength -= 2;
      if (this.strength < 0) {
        this.strength = 0;
      }
      this.condition -= 2;
      if (this.condition < 0) {
        this.condition = 0;
      }

      this._title = "Fattig " + this._title;

      // Need to recalulate hp since abilityes has change.
      this.hp = 50 + Math.round((this.size * this.condition) / 2);
    }
  };

module.exports = creature;
