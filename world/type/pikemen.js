"use strict";

const { red, green, bold } = require("../../game/style.js");
const action = require("../../game/action.js");

let creature = base =>
  class pikemen extends base {
    constructor(name) {
      super(name);
      this.cost += 100;
      this.name = name;
      this._description = "";
      this._salute = "";
      this._lose = "";
      this._win = "";
    }

    pike(enemy) {
      const _this = this;

      const my_action = new action("Ett stick!");
      my_action.type.push("steel");
      my_action.by = _this;
      my_action.target = enemy;
      my_action.maxDamage =
        Math.round((this.strength * this.strength) / 2) + 30;
      my_action.minDamage = 30;
      my_action.normalDistribution = 2;
      my_action.initiativ = this.flexibility / 2;
      my_action.accuracy = (4 * this.flexibility) / 100;
      return my_action;
    }
  };

module.exports = creature;
