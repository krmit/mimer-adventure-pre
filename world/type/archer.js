"use strict";

const { red, green, bold } = require("../../game/style.js");
const action = require("../../game/action.js");

let creature = base =>
  class archer extends base {
    constructor(name) {
      super(name);
      this.cost += 500;
      this.name = name;
      this._description = "";
      this._salute = "";
      this._lose = "";
      this._win = "";
      this._title = "Bågskytt";
    }

    shoot(enemy) {
      const _this = this;

      const my_action = new action("En pil!");
      my_action.type.push("steel");
      my_action.by = _this;
      my_action.target = enemy;
      my_action.maxDamage =
        Math.round((this.strength * this.flexibility) / 2) + 20;
      my_action.minDamage = 20;
      my_action.normalDistribution = 2;
      my_action.initiativ = this.flexibility + 2;
      my_action.accuracy = 0.5 + (4 * (this.flexibility + this.strength)) / 100;
      return my_action;
    }
  };

module.exports = creature;
