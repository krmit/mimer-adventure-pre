"use strict";
const { red, green, bold } = require("../../game/style.js");
const action = require("../../game/action.js");

let creature = base =>
  class knight extends base {
    constructor(name) {
      super(name);
      this.cost += 500;
      this.name = name;
      this._description =
        "Riddaren är en mäktig krigare. " +
        "Ridaren gör stor skada genom att hugga med sitt " +
        "svärd. Rustningen skyddar mot olika attacker.";
      this._salute = "Jag ska rädda dig ifrån detta hemska monster! 😇";
      this._lose = "Ack och ve, onskan vinner igen i denna onda värld!";
      this._win = "Rättvisan har segrat!";
      this._title = "Riddare";
    }

    cut(enemy) {
      const _this = this;

      const my_action = new action("Ett hugg!");
      my_action.type.push("steel");
      my_action.by = _this;
      my_action.target = enemy;
      my_action.maxDamage = this.strength * 2 + 10;
      my_action.minDamage = this.strength + 10;
      my_action.normalDistribution = 2;
      my_action.initiativ = this.flexibility + 2;
      my_action.accuracy = 0.5 + (2 * (this.flexibility + this.strength)) / 100;
      return my_action;
    }

    cut_info() {
      const cut = this.cut(null);
      return (
        "Ett kraftigt hugg. " +
        cut.minDamage +
        "-" +
        cut.maxDamage +
        " ❤️ " +
        Math.floor(cut.accuracy * 100) +
        "% 🎯"
      );
    }

    fendOff(enemy) {
      const _this = this;
      this._protection = 0.6 + (2 * this.flexibility) / 100;

      const my_action = new action("Jag parerar din onska", ["steel"], _this);

      return my_action;
    }

    fendOff_info() {
      this.fendOff(null);
      const protect = this._protection;
      this._protection = 0;
      return "En skicklig parad. -" + Math.floor(protect * 100) + "% 🎯";
    }
  };

module.exports = creature;
