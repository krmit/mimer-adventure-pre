"use strict";

const { red, green, bold } = require("../../game/style.js");

let creature = base =>
  class nobel extends base {
    constructor(name) {
      super(name);
      this.cost += 250;
      this.size += 2;
      this.strength += 2;
      this.condition += 2;
      this.title = "Ädel " + this.title;

      // Need to recalulate hp since abilityes has change.
      this.hp = 50 + Math.round((this.size * this.condition) / 2);
    }
  };

module.exports = creature;
