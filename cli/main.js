#!/usr/bin/node
"use strict"

const fs = require("fs-extra");
const moment = require("moment");
const jsome = require("jsome");
const yaml = require("js-yaml");
const sprintf = require('sprintf-js').sprintf;
const player = require("./player.js");
const engine = require("../game/engine.js");
const world = require("../world/register.js");

world.player.local = require("./player.js").player;
world.player.aiRandom = require("../ai/player-ai-random.js").player;

const game = new engine(world.player.local, world);

