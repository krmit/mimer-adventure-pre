"use strict"

let colors = require("colors/safe");
let prompt = require("promptly");
const moment = require("moment");
const sprintf = require('sprintf-js').sprintf;
const {player} = require("../game/player.js");

const _convert = function(words) {
			let line = "";
		for(let word of words) {
			if(typeof word === "string") {
			    line+=word;
			}
			else if(Array.isArray(word)) {
				line+=_convert(word);
			}
			else {
				let text = word.msg;
			    if(word.color !== undefined) {
					text = colors[word.color](text);
				}
				 if(word.fontWeight !== undefined) {
					 text = colors[word.fontWeight](text);
				}
                line+=text;
			}
	} 
	return line;
}

module.exports.player = class playerCLI extends player {

  constructor(name) {
    super(name);
    this._type="local";
  }
    static info() {
		return "A local player";
	}
	
	write(...words) {
		// Ignoring option for short message.
	    if(typeof words[words.length-1] === "boolean") {
			words.pop();
		}
		
	    let line = _convert(words);
	    
	    
		console.log(line);
		return Promise.resolve(true);
		
	}
	
	send(...msg){
		for(let p of player.all) {
			if(!(p.constructor.name === "playerCLI" && p._name !== "Host")) {
		        p.write(...msg);
		    }	
		}
	};
	
	question(...msg) {
		
		// add prmist to earlyer cli promise.
		
		playerCLI._before = playerCLI._before.then(()=> {
		if(msg.length!==0){
		    console.log(_convert(msg));
	    }	
		for(let i = 0; i <  this._alternatives.length; i++) {
			console.log(" "+ colors.bold(i)+"  " + _convert(this._alternatives[i].msg));
		}
		const result = this._alternatives;
		this._alternatives=[];
	    return prompt.prompt(">").then((selection)=>result[selection].callback());
	});
	return	playerCLI._before;
}

	input(msg, defultValue) {
		let text = 0;
		if(msg.length!==0){
			text = _convert(msg);
		}
	 return playerCLI._before = playerCLI._before.then(()=> {	
	     return prompt.prompt(text+" ("+defultValue+"):", { default: defultValue });	
     });
     
	}
}

module.exports.player._before = Promise.resolve();
