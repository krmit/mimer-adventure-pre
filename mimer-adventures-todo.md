# För att vara användbart

[Mimer äventyr](http://htsit.se/p)

## Steg 1:

1.  Fixa inputen
2.  Gör så tt texten stannar upp ibland.
3.  Se till att vi får bra utskrifter.
4.  Gör så någon vinner samt en vinnar utskrift.
5.  Gör en bättre damage beräknare.
6.  Gör så info om vara action finns tillgänglig.

## Steg 2:

7.  Få cli versionen att fungera igen.
8.  Få Typescript att fungera i dev.
9.  Göt kontrakt för individer
10. Få Typescript att fungera i experiment
11. Skapa en action klass.
12. Designa om hur saker som påverkar den som gör en action behandlas.
13. Designa om damages metoden för humanoid.
14. Integrera nya varelser.

## Steg 3:

14. Load/save game/config i början av spelet.
15. Load/save creature.
16. Lpoad/save player
17. Varje spelare kan ha flera creature.
18. Varje acction måste välja en target.
19. Vi kan ha flera spelara i varje battle.
20. Vi kan spela via nätverk.

## Möjliga framtida senarior.

11. Närhete, som begränsar vilka som kan vara en target.
12. Utrustning som vapen, som påverkar spelarna statestik.
13. Ett mer advancerat sätt att skapa spelar, som ger spelaren mer kontrol, men också så olika spelare blir mer balanserade och mindre tur.
14. En affär där splarna kan köpa saker.
15. Gör så att altid ett alternativ är default och går att trycka sig förbi med enter.
16. Skriv ommin text hantering så style är en class som extens Array.
