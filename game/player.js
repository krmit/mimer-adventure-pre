"use strict"

let colors = require("colors/safe");
let prompt = require("promptly");
const moment = require("moment");
const sprintf = require('sprintf-js').sprintf;
const {red, green, bold} = require("../game/style.js");


module.exports.player = class player {

  constructor(name) {
    this._alternatives=[];
	this._name=name;
	this._type="none";
	this._description="none";
	player.all.push(this);
  }
	
	status() {
	    let line = ["A ", this._type, " player with the name of ", this._name];
		return line;
	}
	
	nameLabel() {
	    let line = [bold("Spelare: "), bold().red(this._name)];
		return line;
	}
	
	write(...words) {}
	
	add(id, callback, ...words) {
		const options = {};
		if(words[0].msg !== undefined) {
		    options = words.shift();
	    }
		this._alternatives.push({
			 "id": id, 
		     "msg": words,
		     "callback": callback,
		     "options": options 
	});
	}
	
	question(msg) {}	
}

module.exports.player.all=[];
