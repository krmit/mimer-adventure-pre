"use strict"

module.exports = class action {

	constructor(msg="",type=[],by=null,target=null,initiativ=0,accuracy=1,minDamage=0,maxDamage=10,normalDistribution=1, heal=false) {
			this.msg=msg;
            this.type=type;
            this.by=by;
            this.target=target;
            this.initiativ=initiativ;
            this.accuracy=accuracy;
            this.minDamage=minDamage;
            this.maxDamage=maxDamage;
            this.normalDistribution=normalDistribution;
            this.heal=heal;	
	}
	
}
