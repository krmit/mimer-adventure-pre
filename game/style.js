"use strict"

require("./style.js");

let style = {
	red: mod("color", "red"),
	blue: mod("color", "blue"),
	green: mod("color", "green"),
	yellow: mod("color", "yellow"),
	bold: mod("fontWeight", "bold")
}

function mod(type, value) {
  return function(...msg) {
	 let result;
	 if(this === undefined || this.msg === undefined) {
		result =  Object.assign({},style);
		result.msg="";
	 }
	 else {
		result=this;
	 }

	  if(msg !== undefined) {
		result.msg=msg.join("");
	  }
	  
	 result[type] = value;
	 return result;
  }
}

module.exports = style;



