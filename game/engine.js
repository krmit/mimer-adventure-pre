"use strict"
const {blue, red, bold} = require("./style.js");

function checkForFaint(p1, p2, output) {
	const status = p1.status();
	if(p1.isFighting()) {
		output.send(status);
		return true;
	} else {
		output.send(status);
		output.send(bold("\n", "Striden är slut!", "\n"));
		output.send(p1.lose());
		output.send(p2.win());
		return false;
	}
}

module.exports = function(hostClass, world) {
	const host = new hostClass("Host");
	this.turnNumber = 0;
	this.numberOfPlayers=2;
	
	host.write(bold().red("\n", "🔥 Startar spel Mimer Äventyr 🔥 🤠"), false);
	
	let selection_promise= Promise.resolve([]);
	let namn_list = ["Svea", "Bob", "Adam", "Jane"]
	const players=[];
	
	for(let i =0; i < this.numberOfPlayers; i++) {
	selection_promise = selection_promise.then((players) => {
	host.write(bold(`\nSpelare: ${i+1}\n`), false);

	return host.input([bold("❓ Vad är namnet på spelaren?")], namn_list.pop()).then((name)=> {
		console.log("hej!!!!!!!!!!!!!!!");
	for(let player_key of Object.keys(world.player).sort()) {
		let player = world.player[player_key];
	    host.add(player_key, function(){return new player(name)}, player.info());
    }
  
    return host.question([bold("❓ Vem ska spela?")]).then((player)=>{
	return host.input([bold("❓ Vad är namnet på din varelse?")], namn_list.pop()).then((name)=> {
	
	for(let creature_name of Object.keys(world.creatures).sort()) {
		let creature = world.creatures[creature_name];
	    host.add(creature_name, function(){return creature.createRandom(name)}, creature.infoChooseMe());
    }
    return host.question([bold("❓ Vem vill du vara?")]).then((creature) => {
		players.push({player,creature});
		return players;
	});
	});
});
});
}); 
}

	selection_promise.then((players)=> {
	for(let i =0; i < this.numberOfPlayers; i++) {
	host.write(bold(`Spelare ${i+1}:\n`) , players[i].player.status(), "\n", players[i].creature.statusOfStats(), players[i].creature.salute());
    }
	this.turn(players);
	
})

  
  this.turn = function(players) {
	this.turnNumber++;	
	let _this = this;
	let turn_player_list = [];
	
    host.send("\n", bold("Turn: "), String(this.turnNumber),"\n");
	
	// Select actions
	for(let i =0; i < this.numberOfPlayers; i++) {
	
	const numberOfPlayers = this.numberOfPlayers;
	turn_player_list.push(
	new Promise(function(resolve, reject) {
	    let target;
	    if(i+1 !== numberOfPlayers) {
	     target = players[i+1].creature;
	    } else {
		   target = players[0].creature;	
		}
		for(let alt of players[i].creature.moves(target)) {
			players[i].player.add(...alt);
		}
		resolve(players[i].player.question(players[i].player.nameLabel(), "\n", players[i].creature.ask(), "\n"));
	}));
	}
	// Sort for initiativ
	
	 Promise.all(turn_player_list).then((data)=>{
     data.sort(function(a,b) {
        return a.initiativ - b.initiativ;
     }).reverse();
     
     

         
     for(const action of data) {
     		if(action.do !== undefined) {
			action.do();
		}
	 }
	 
     for(const action of data) {
		let msg_list=[action.by.nameTag(), action.msg];

		if(action.target !== null) {
		    let msg = action.target.damage(action);
		    msg_list.push("\n");
		    msg_list.push(action.target.nameTag());
		    msg_list.push(msg);
	}
	  host.send(msg_list);
	 };
	 

	 host.send("\n",bold("Status"),"\n", false);
	 let p1 = players[0].creature.isFighting();
	 let p2 = players[1].creature.isFighting();
	 if(!p1) {
		host.send("\n😵😵😵😵😵\n",players[0].creature.lose(),"\n😵😵😵😵😵\n", false);
	 }
	 if(!p2) {
		host.send("\n😵😵😵😵😵\n",players[1].creature.lose(),"\n😵😵😵😵😵\n", false);
	 }
	 if( p1 && p2 ) {
	   host.send(players[0].creature.status(),"\n", players[1].creature.status(), false);
	   this.turn(players);
   }
   else {
	   if(!(p1 || p2)) {
		host.send("❗️Oavgjort❗ ❓❓❓❓", false);
		host.send("\n", blue("👋  Slut på spelet 👋 "), "\n", false); 
	   }
	   else {
	   if(p1){
		   host.send("\n🎉🎉🎉🎉🎉🎉\n", players[0].creature.win(), "\n🎉🎉🎉🎉🎉\n", false);
		   host.send("\n🎉Vinaren är:🎉\n🥇 ", players[0].player.nameLabel(), " 👑", false);
	   }
	   else {
	       host.send("\n🎉🎉🎉🎉🎉🎉\n", players[1].creature.win(), "\n🎉🎉🎉🎉🎉\n", false);
		   host.send("\n🎉Vinaren är:🎉\n🥇 ", players[0].player.nameLabel(), " 👑", false);
	   }
	   host.send("\n", blue("👋  Slut på spelet 👋 "), "\n", false);
   }
   }
 });
}

}
